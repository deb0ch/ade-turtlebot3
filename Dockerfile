# An ADE-compatible image
FROM registry.gitlab.com/deb0ch/ade-ubuntu/focal-extra:latest


# Install ROS 2
RUN apt-get -y update && apt-get -y install \
    curl                                    \
    git                                     \
    gnupg2                                  \
    lsb-release                             \
    unzip                                   \
    && rm -rf /var/lib/apt/lists/*
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -
RUN sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'
RUN apt-get -y update && apt-get -y install \
    ros-foxy-desktop                        \
    ros-foxy-gazebo-ros-pkgs                \
    ros-foxy-eigen3-cmake-module            \
    python3-argcomplete                     \
    python3-colcon-common-extensions        \
    python3-vcstool                         \
    python3-rosdep                          \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get -y update && apt-get -y install \
    python3-pip                             \
    && rm -rf /var/lib/apt/lists/*
RUN rosdep init
  
ENV GAZEBO_MODEL_PATH=/usr/share/gazebo/models

RUN git clone --depth=1 https://github.com/osrf/gazebo_models $GAZEBO_MODEL_PATH \
    && rm -rf $GAZEBO_MODEL_PATH/.git


# Turtlebot3: install cartographer
RUN apt-get -y update && apt-get -y install \
    google-mock                             \
    libboost-dev                            \
    libboost-iostreams-dev                  \
    libcairo2-dev                           \
    libceres-dev                            \
    liblua5.3-dev                           \
    libpcl-dev                              \
    libprotobuf-dev                         \
    protobuf-compiler                       \
    python3-sphinx                          \
    ros-foxy-cartographer                   \
    ros-foxy-cartographer-ros               \
    ros-foxy-nav2-bringup                   \
    ros-foxy-navigation2                    \
    ros-foxy-dynamixel-sdk                  \
    ros-foxy-hls-lfcd-lds-driver            \
    && rm -rf /var/lib/apt/lists/*

##
## Copy local files into the system
##

COPY skel/. /