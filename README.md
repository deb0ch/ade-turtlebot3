# minimal-ade

A minimal example of an ade-compatible project:

```
~/ade-home/
├── .adehome <- Tells ade-cli that this is the root of this ade home
└── minimal-ade
    ├── .aderc <- Tells ade-cli what images and volumes to use
    ├── Dockerfile
    ├── entrypoint
    └── env.sh
```

Turtlebot3 is navigating using ros foxy. 

## Build and start

- Create `ade-home` (if not created)
```
$ cd ~
$ mkdir adehome-turtlebot3
$ cd adehome-turtlebot3
$ touch .adehome
```
- **Note:** The `ade-home` directory can have any name, it just needs to contain `.adehome`

- Clone this repository, build the docker image, and start ade
```
$ cd ~/adehome-turtlebot3/
$ git clone https://gitlab.com/alia2/ade-turtlebot3.git
$ cd ~/adehome-turtlebot3/ade-turtlebot3
$ ade start
$ ade enter
```
You can now run the demo:

```shell
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:~/turtlebot3_ws/src/turtlebot3/turtlebot3_simulations/turtlebot3_gazebo/models
export TURTLEBOT3_MODEL=burger
source ~/turtlebot3_ws/install/setup.$(basename $0)
ros2 launch turtlebot3_gazebo turtlebot3_world.launch.py
```



