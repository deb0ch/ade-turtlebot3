#
# This file is meant to be run using the steps utility
#

mkdir -p ~/turtlebot3_ws/src
cd ~/turtlebot3_ws/src && vcs import < ~/turtlebot3.repos

#build workspace
cd ~/turtlebot3_ws && source /opt/ros/foxy/setup.sh && colcon build --executor sequential --event-handlers console_direct+ desktop_notification- status- --cmake-args -DCMAKE_BUILD_TYPE=Release

cat README.md
