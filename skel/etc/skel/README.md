# ADE-Turtlebot3

Launch TB3 with gazebo:

```shell
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:~/turtlebot3_ws/src/turtlebot3/turtlebot3_simulations/turtlebot3_gazebo/models
export TURTLEBOT3_MODEL=burger
source ~/turtlebot3_ws/install/setup.zsh
ros2 launch turtlebot3_gazebo turtlebot3_world.launch.py
```
